set number
set relativenumber

"Begin plugin section
call plug#begin('~/.vim/plugged')

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/denite.nvim'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'rust-lang/rust.vim'
Plug 'bubujka/emmet-vim'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'lifepillar/vim-solarized8'
Plug 'jiangmiao/auto-pairs'
Plug 'w0rp/ale'

call plug#end()

let g:deoplete#enable_at_startup = 1
nmap <F8> :TagbarToggle<CR>
map <C-n> :NERDTreeToggle<CR>

set background=light
colorscheme solarized8 
